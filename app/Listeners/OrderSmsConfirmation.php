<?php namespace App\Listeners;

use Setting;
use SMS;

use App\Models\Control\Order;

use App\Events\OrderWasConfirmed;
use App\Events\OrderWasPayed;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderSmsConfirmation
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event
	 * 
	 * @param  OrderWasConfirmed $event
	 * @return void
	 */
	public function onOrderConfirm(OrderWasConfirmed $event)
	{
		if ( ! env('APP_DEBUG'))
		{
			if ( ! Setting::val('order-notify-after-payment') || (Setting::val('order-notify-after-payment') && $event->order->status == Order::STATUS_PAYED))
			{
				SMS::send(Setting::val('order-phone-notifications'), sprintf(_('Нове замовлення №%s на суму %s'), $event->order->number, $event->order->total));
			}
		}
	}

	/**
	 * Handle the event
	 * 
	 * @param  OrderWasPayed $event
	 * @return void
	 */
	public function onOrderPay(OrderWasPayed $event)
	{
		if ( ! env('APP_DEBUG'))
		{
			if (Setting::val('order-notify-after-payment') && $event->order->status == Order::STATUS_PAYED)
			{
				SMS::send(Setting::val('order-phone-notifications'), sprintf(_('Замовлення №%s оплачено на суму %s'), $event->order->number, $event->order->total));
			}
		}
	}

	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		$events->listen(
			'App\Events\OrderWasConfirmed',
			'App\Listeners\OrderSmsConfirmation@onOrderConfirm'
		);

		$events->listen(
			'App\Events\OrderWasPayed',
			'App\Listeners\OrderSmsConfirmation@onOrderPay'
		);
	}    
}
