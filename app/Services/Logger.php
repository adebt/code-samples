<?php namespace App\Services;

use App\Models\Control\Log;

class Logger {

	/**
	 * Current place
	 * 
	 * @var null
	 */
	public $place_id = null;

	/**
	 * Request object
	 * 
	 * @var Illuminate\Http\Request
	 */
	private $request;

	/**
	 * Init logger
	 * 
	 * @param Illuminate\Http\Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}

	/**
	 * Log information
	 * 
	 * @param  array $data
	 */
	public function log($data)
	{
		$data = $data + [
			'place_id' => $this->place_id,
			'user_id' => (isset($this->auth->id)) ? $this->auth->id : null,
			'url' => $this->request->path(),
			'request' => $this->request->all(),
			'ip' => $this->request->server('REMOTE_ADDR'),
			'location' => (isset($_COOKIE['latitude'])) ? $_COOKIE['latitude'] . ',' . $_COOKIE['longitude'] : null
		];

		(new Log($data))->save();
	}
}
