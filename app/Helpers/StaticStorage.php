<?php namespace App\Helpers;

use File;
use Route;

class StaticStorage {

	protected static $manifest;

	protected static $namespace = 'App\\Http\\Controllers\\';

	protected static $prefix = 'c';

	/**
	 * Return full web path to asset
	 * 
	 * @param  string $path
	 * @return string
	 */
	public static function asset($path, $full=false)
	{
		$namespace = self::ns();
		
		$r = Route::getCurrentRoute();
		$r = $r->getAction();
		
		$prefix = '';
		if ($full)
			$prefix .= 'http://' . $r['domain'];

		if ( !empty( $namespace ) )
			return $prefix . '/r/' . $namespace . '/' . ltrim($path, '/');
		else
			return $prefix . $path;
	}

	/**
	 * Return full web path to compiled asset
	 * 
	 * @param  string $path
	 * @return string
	 */
	public static function compiled($path, $full=false)
	{
		$namespace = self::ns();
		
		$r = Route::getCurrentRoute();
		$r = $r->getAction();
		
		$prefix = '';
		if ($full)
			$prefix .= 'http://' . $r['domain'];

		if ( !empty( $namespace ) )
			return $prefix . self::getCompiledName('r/' . $namespace . '/' . $path);
		else
			return $prefix . $path;
	}

	/**
	 * Return current namespace
	 * 
	 * @return string
	 */
	public static function ns()
	{
		$r = Route::getCurrentRoute();

		if (is_object($r))
		{
			$r = $r->getAction();
			return mb_strtolower(str_replace(self::$namespace, '', $r['namespace']));
		}
		else
		{
			return '';
		}
	}

	/**
	 * Get cache prefix
	 * 
	 * @param  string $filename
	 * @return string
	 */
	public static function getCachePrefix($filename)
	{
		return substr(md5_file($filename), 0, 6);
	}	

	/**
	 * Get Compiled Name
	 * 
	 * @param  string $file
	 * @param  string $namespace
	 * @return string
	 */
	private static function getCompiledName($file)
	{
		if (env('APP_DEBUG') && file_exists(public_path() . '/' . $file))
		{
			return '/' . $file;
		}
		else
		{
			$manifest = json_decode(file_get_contents(public_path().'/rev-manifest.json'), true);

			if (isset($manifest[$file])) {
				return '/' . $manifest[$file];
			}

			throw new InvalidArgumentException("File {$file} not defined in asset manifest.");
		}
	}

}
