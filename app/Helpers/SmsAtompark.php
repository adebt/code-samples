<?php namespace App\Helpers;

class SmsAtompark {

	/**
	 * Delay between SMS send (in seconds)
	 *
	 * @var  integer
	 */
	const SEND_DELAY = 90;

	/**
	 * public key
	 * 
	 * @var string
	 */
	protected $public_key = 'public key';

	/**
	 * private key
	 * 
	 * @var string
	 */
	protected $private_key = 'private key';

	/**
	 * API Version
	 * 
	 * @var string
	 */
	protected $version = '3.0';

	/**
	 * API URL
	 * 
	 * @var string
	 */
	protected $url = 'http://atompark.com/api/sms/';

	/**
	 * API Sender
	 * 
	 * @var string
	 */
	protected $sender = 'sender';

	/**
	 * API Test state
	 * 
	 * @var integer
	 */
	protected $test = 0;

	/**
	 * Return full web path to asset
	 * 
	 * @param  string $path
	 * 
	 * @return string
	 */
	public static function send($phone, $message)
	{
		$instance = self::getInstance();

		return $instance->execCommand( 'sendSMS', ['phone' => $phone, 'text' => $message, 'datetime' => '', 'sms_lifetime' => 0] );
	}

	/**
	 * Get Class Instance
	 * 
	 * @return SmsAtompark
	 */
	private static function getInstance()
	{
		return new SmsAtompark();
	}

	/**
	 * Exec API Command
	 * 
	 * @param  string $command
	 * @param  array $params
	 * 
	 * @return bool
	 */
	private function execCommand($command, $params)
	{
		$url = $this->url . $this->version . '/' . $command;

		if ( $this->test )
			$params['test'] = $this->test;

		$params['sender'] = $this->sender;
		$params['key'] = $this->public_key;
		$params['sum'] = $this->calculateControlSum($params, $command);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_URL, $url);
		$response = curl_exec($ch);

		$response = @json_decode( $response, true );

		if ( isset($response['result']['id']) )
			return true;
		else
			return false;
	}

	/**
	 * Calculate Control SUM
	 * 
	 * @param  array $params
	 * @param  string $action
	 * 
	 * @return string
	 */
	private function calculateControlSum($params, $action)
	{
		$params['version'] = $this->version;
		$params['action'] = $action;

		ksort($params);

		return md5(implode('', $params) . $this->private_key);
	}
}
