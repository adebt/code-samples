<?php namespace App\Http\Controllers\Control;

use Cache;
use View;

use Illuminate\Http\Request;

use App\Http\Requests\Control\Category\CreateRequest;
use App\Http\Requests\Control\Category\UpdateRequest;

use App\Models\Control\Category;
use App\Models\Control\Package;
use App\Models\Control\ComponentGroup;

class CategoryController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		if (is_null($this->active_place))
			abort('400');

		View::share('sidebar_category_active', 'categories');
	}

	/**
	 * Show categories list
	 *
	 * @return Response
	 */
	public function getList(Request $request)
	{
		$categories = with(new Category)->listWithPlaceId($this->active_place->id, $request->all(), config('staff.pagination.default'));

		$this->assign = [
			'categories' => $categories,
			'filter' => [
				'parent' => with(new Category)->prepareCategories($this->active_place->id)
			]
		];

		return view('control.categories.list', $this->assign);
	}

	/**
	 * Show category creation form
	 * 
	 * @return Response
	 */
	public function getCreate()
	{
		$this->assign = [
			'categories' => (['' => _('Це батьківська категорія')] + with(new Category)->prepareCategories($this->active_place->id)),
			'packages' => ['' => _('Немає власної упаковки')] + with(new Package)->preparePackages($this->active_place->id),
			'groups' => ['' => _('Немає додаткових компонентів')] + with(new ComponentGroup)->prepareGroups($this->active_place->id),
		];

		return view('control.categories.create', $this->assign);
	}
	
	/**
	 * Save new category
	 * 
	 * @param  Category\CreateRequest $request   
	 * @return Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
	 */
	public function postCreate(CreateRequest $request)
	{
		$input = $request->all();

		$input['place_id'] = $this->active_place->id;

		if (empty($input['parent_id']))
			$input['parent_id'] = null;

		if (empty($input['package_id']))
			$input['package_id'] = null;

		$category = new Category( $input );
		$category->save();

		Cache::tags('common', 'menu')->flush();

		return redirect(action('CategoryController@getList'))
			->withMessagesSuccess([
				_('Категорія успішно створена')
			]);
	}

	/**
	 * Show category update form
	 * 
	 * @param  App\Models\Control\Category $category
	 * @return Response
	 */
	public function getUpdate(Category $category)
	{
		$data = $category->toArray();

		$this->assign = [
			'data' => $data,
			'categories' => (['' => _('Це батьківська категорія')] + with(new Category)->prepareCategories($this->active_place->id, $category->id)),
			'packages' => ['' => _('Немає власної упаковки')] + with(new Package)->preparePackages($this->active_place->id),
			'groups' => ['' => _('Немає додаткових компонентів')] + with(new ComponentGroup)->prepareGroups($this->active_place->id),
		];

		return view('control.categories.update', $this->assign);
	}

	/**
	 * Update category
	 * 
	 * @param  Category\CreateRequest $request   
	 * @param  App\Models\Control\Category $category
	 * @return Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
	 */
	public function postUpdate(UpdateRequest $request, Category $category)
	{
		$input = $request->all();
		if (empty($input['parent_id']))
			$input['parent_id'] = null;

		if (empty($input['package_id']))
			$input['package_id'] = null;

		$category->fill( $input );
		$category->push();

		Cache::tags('common', 'menu')->flush();

		return redirect(action('CategoryController@getList'))
			->withMessagesSuccess([
				_('Дані категорії оновлені')
			]);
	}

	/**
	 * Delete category
	 * 
	 * @param  App\Models\Control\Category $category
	 * @return Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
	 */
	public function getDelete(Category $category)
	{
		$category->delete();

		Cache::tags('common', 'menu')->flush();

		return redirect(action('CategoryController@getList'))
			->withMessagesSuccess([
				_('Категорію видалено')
			]);
	}
}
