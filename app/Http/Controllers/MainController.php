<?php namespace App\Http\Controllers\Clients;

use Auth;
use Cache;
use Cookie;
use View;

use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;

use App\Http\Requests\Clients\Review\AddRequest;

use App\Models\Control\Category;
use App\Models\Control\Dish;
use App\Models\Control\DishComponent;
use App\Models\Control\DishPortion;
use App\Models\Control\Review;

class MainController extends Controller {

	/**
	 * Cache tag for this module
	 */
	const CACHE_TAG = 'menu';

	/**
	 * Constructor
	 * 
	 * @param CookieJar $cookieJar
	 */
	public function __construct(CookieJar $cookieJar)
	{
		parent::__construct($cookieJar);

		View::share('active_module', 'main');

		if (Auth::check())
			View::share('favorite', ['dishes' => $this->auth->getFavoriteDishes($this->active_place->id, 'id')]);
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @param  string $zone
	 * @return Response
	 */
	public function index($zone, CookieJar $cookieJar, Request $request)
	{
		if ( ! is_null($request->input('clear')))
		{
			$cookieJar->queue($cookieJar->forget('category'));
			$cookieJar->queue($cookieJar->forget('dish'));
		}

		$cookieJar->queue(cookie('base', '', 365 * 24 * 60));

		$cache_key = 'category/main/' . $this->sort;
		$this->assign = Cache::tags(self::CACHE_TAG)->remember($cache_key, config('staff.cache.lifetime.menu'), function()
		{
			$categories = with(new Category)->listActiveWithPlaceId( $this->active_place->id, null );

			if ( ! is_null($this->sort))
				(new Category)->sortDishes($categories, $this->sort);

			return ['categories' => $categories];
		});

		return view($this->active_place->slug . '.main.index', $this->assign);
	}

	/**
	 * Get category
	 * 
	 * @param  string   $zone
	 * @param  Category $category
	 * @return Response
	 */
	public function getCategory($zone, CookieJar $cookieJar, Request $request, Category $category)
	{
		if ($category->place_id != $this->active_place->id)
			abort('404');
		
		$cookieJar->queue(cookie('base', $category->id, 365 * 24 * 60));

		$cache_key = 'category/' . $category->id . '/' . $this->sort;
		$this->assign = Cache::tags(self::CACHE_TAG)->remember($cache_key, config('staff.cache.lifetime.menu'), function() use ($category)
		{
			$categories = with(new Category)->listActiveWithPlaceIdAndParentId( $this->active_place->id, $category->id, null );

			if ( ! is_null($this->sort))
			{
				$category = collect([$category]);

				(new Category)->sortDishes($categories, $this->sort);
				(new Category)->sortDishes($category, $this->sort);

				$category = $category->first();
			}

			return [
				'categories' => $categories,
				'parent' => $category,
			];
		});

		return view($this->active_place->slug . '.main.index', $this->assign);
	}

	/**
	 * Get favorites list
	 * 
	 * @param  string    $zone
	 * @param  CookieJar $cookieJar
	 * @return Response
	 */
	public function getFavorites($zone, CookieJar $cookieJar)
	{
		if ( ! Auth::check())
			return redirect(action('MainController@index', ['zone' => $zone]));

		$cookieJar->queue(cookie('base', 'favorite', 365 * 24 * 60));
		
		$cache_key = 'category/favorite/' . $this->auth->id . '/' . $this->sort;
		$this->assign = Cache::tags(self::CACHE_TAG)->remember($cache_key, config('staff.cache.lifetime.menu'), function()
		{
			$category = new Category;
			$category->id = 'favorite';
			$category->name = ['uk_UA' => _('Улюблені')];
			$category->dishes = $this->auth->getFavoriteDishes($this->active_place->id);

			if ( ! is_null($this->sort))
			{
				$category = collect([$category]);
				(new Category)->sortDishes($category, $this->sort);
				$category = $category->first();
			}

			return [
				'parent' => $category,
				'categories' => []
			];
		});		

		return view($this->active_place->slug . '.main.index', $this->assign);		
	}

	/**
	 * Get Dish
	 * 
	 * @param  string $zone
	 * @param  Dish   $dish
	 * @return Response
	 */
	public function getDish($zone, Request $request, Dish $dish)
	{
		if ($dish->place_id != $this->active_place->id || ! $dish->is_active)
			abort('404');

		$base = Cookie::get('base');
		if ( is_integer($base))
			$back = action('MainController@getCategory', ['zone' => $zone, 'category' => $base]);
		else if ($base == 'favorite')
			$back = action('MainController@getFavorites', ['zone' => $zone]);
		else
			$back = action('MainController@index', ['zone' => $zone]);

		View::share('active_module', '');

		$components = [];
		foreach ($dish->getComponents(true) as $component)
			$components[] = mb_strtolower($component->component->name[config('laravel-gettext.locale')]);

		$components = implode(', ', $components);

		$dish->reviews = $dish->reviews()->latest()->get();
		$dish->load(['portions','components','reviews.comments', 'reviews.comments.user', 'reviews.user', 'rating', 'favorite']);

		$review = false;
		if( ! is_null($request->input('review')))
			$review = true;

		$in_favorite = false;
		$my_rate = 0;
		if (Auth::check())
		{
			$in_favorite = (bool)$dish->wasFavoritedByUser($this->auth->id);
			$my_rate = $dish->wasRatedByUser($this->auth->id);

			if ( ! empty($my_rate))
				$my_rate = (int)$my_rate->rating;
		}

		$this->assign = [
			'dish' => $dish,
			'back' => $back,
			'components' => $components,
			'review' => $review,
			'in_favorite' => $in_favorite,
			'my_rate' => $my_rate
		];

		return view($this->active_place->slug . '.main.dish', $this->assign);
	}

	/**
	 * Save dish review
	 *
	 * @return Response
	 */
	public function postReview($zone, AddRequest $request, Dish $dish)
	{
		if ($dish->place_id != $this->active_place->id)
			abort('404');

		if (Auth::check() && $this->auth->canPost())
		{
			$review = new Review([
				'place_id' => $this->active_place->id,
				'user_id' => $this->auth->id,
				'text' => $request->input('text')
			]);

			$dish->reviews()->save($review);

			Cache::tags(self::CACHE_TAG)->flush();

			return redirect()->back()
				->withMessagesSuccess([_('Ваш відгук було успішно збережено')]);
		}
		else
		{
			return redirect()->back()
				->withErrors([_('У вас немає прав писати відгуки')]);
		}
	}

	/**
	 * Save dish rating
	 *
	 * @return Response
	 */
	public function postRating($zone, Request $request, Dish $dish)
	{
		if ($dish->place_id != $this->active_place->id)
			abort('404');

		if (Auth::check() && $request->input('rating') >= 0 && $request->input('rating') <= 5)
		{
			$rating = [
				'place_id' => $this->active_place->id,
				'user_id' => $this->auth->id,
				'rating' => $request->input('rating')
			];

			if ($request->input('rating') == 0)
			{
				$dish->rating()
					->where('user_id', '=', $rating['user_id'])
					->delete();
			}
			else
			{
				$was_rated = $dish->wasRatedByUser($this->auth->id);

				if (count($was_rated))
					$was_rated->update($rating);
				else
					$dish->rating()->create($rating);
			}

			Cache::tags(self::CACHE_TAG)->flush();

			return json_encode(['rating' => $dish->recalcRating()]);
		}

		return json_encode(['rating' => $dish->rating]);
	}

	/**
	 * Add dish to favorite
	 * 
	 * @param  string  $zone
	 * @param  Request $request
	 * @param  Dish    $dish
	 * @return Response
	 */
	public function postFavorite($zone, Request $request, Dish $dish)
	{
		if ($dish->place_id != $this->active_place->id)
			abort('404');

		if (Auth::check())
		{
			$favorite = [
				'place_id' => $this->active_place->id,
				'user_id' => $this->auth->id,
			];

			$was_favorited = $dish->wasFavoritedByUser($this->auth->id);

			if (count($was_favorited))
				$was_favorited->delete();
			else
				$dish->favorite()->create($favorite);

			Cache::tags(self::CACHE_TAG)->flush();

			return json_encode(['favorite' => !(bool)$was_favorited]);
		}
	}
}
