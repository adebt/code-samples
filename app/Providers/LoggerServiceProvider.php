<?php namespace App\Providers;

use Request;

use Illuminate\Support\ServiceProvider;

class LoggerServiceProvider extends ServiceProvider {

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('Logger', function($app)
		{
			return new \App\Services\Logger( Request::instance() );
		});
	}
}
