<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SettingServiceProvider extends ServiceProvider {

	/**
	 * Register basket application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('setting', function($app)
		{
			return new \App\Models\Control\Settings;
		});
	}
}
