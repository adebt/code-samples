<?php namespace App\Providers;

use Illuminate\Html\HtmlServiceProvider as BaseHtmlServiceProvider;
use App\Extend\Html\FormBuilder;

class HtmlServiceProvider extends BaseHtmlServiceProvider {

	/**
	 * Register the form builder instance.
	 *
	 * @return void
	 */
	protected function registerFormBuilder()
	{
		$this->app->bindShared('form', function($app)
		{
			$form = new FormBuilder($app['html'], $app['url'], $app['session.store']->getToken());

			return $form->setSessionStore($app['session.store']);
		});
	}

}
