<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'SocialiteProviders\Manager\SocialiteWasCalled' => [
			'SocialiteProviders\VKontakte\VKontakteExtendSocialite@handle',
			'SocialiteProviders\Instagram\InstagramExtendSocialite@handle',
			'JhaoDa\SocialiteProviders\Odnoklassniki\OdnoklassnikiExtendSocialite@handle'
		],
	];

	/**
	 * The event subscribe mappings for the application.
	 *
	 * @var array
	 */
	protected $subscribe = [
		'App\Listeners\OrderEmailConfirmation',
		'App\Listeners\OrderPrintConfirmation',
		'App\Listeners\OrderSmsConfirmation',
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		//
	}

}
