<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TaskServiceProvider extends ServiceProvider {

	/**
	 * Register basket application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('task', function($app)
		{
			return new \App\Models\Control\Task;
		});
	}
}
