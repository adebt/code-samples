<?php namespace App\Console\Commands;

use File;

use App\Helpers\StaticStorage;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CompileAssets extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'assets:compile';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Compile assets';

	/**
	 * Cache prefix
	 * 
	 * @var string
	 */
	protected $prefix = 'c';

	/**
	 * Cached pathes
	 * 
	 * @var array
	 */
	protected $path = [
		'r/solo/css' => 'css',
		'r/solo/js' => 'js'
	];

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info( 'Copy styles/scripts' );

		foreach ($this->path as $folder => $ext)
		{
			$directory = public_path() . '/' . $folder . '/';
			$compiled_mask = $directory . $this->prefix . '.*.' . $ext;

			$this->info( 'Clear old styles/scripts: ' . $folder );

			$compiled = File::glob($compiled_mask);
			if ( ! empty($compiled))
				File::delete($compiled);

			$this->info( 'Copy new styles/scripts: ' . $folder );

			$prepared_mask = $directory . '*.' . $ext;
			$prepared = File::glob($prepared_mask);

			foreach ($prepared as $file)
			{
				$compile_name = $directory . $this->prefix . '.' . StaticStorage::getCachePrefix($file) . '.' . File::name($file) . '.' . $ext;
				File::copy($file, $compile_name);
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}
}
