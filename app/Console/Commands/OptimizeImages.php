<?php namespace App\Console\Commands;

use File;

use App\Helpers\StaticStorage;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class OptimizeImages extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'images:optimize';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Optimize Images';

	/**
	 * Cache optimize tool
	 * 
	 * @var string
	 */
	protected $bin = 'jpegoptim --all-progressive --strip-all ';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info( 'Optimizing images' );

		$path = rtrim(public_path(), '/') . '/u/';
		$files = File::allFiles($path);

		foreach ($files as $file)
			exec($this->bin . $path . $file->getRelativePathname());

		$this->info( 'Finish' );
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}
}
