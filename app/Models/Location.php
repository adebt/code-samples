<?php namespace App\Models\Control;

use DB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'locations';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['place_id', 'user_id', 'name', 'description', 'address', 'phone', 'email', 'map_url', 'location', 'sort', 'is_active'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['id', 'place_id', 'user_id', 'deleted_at', 'created_at', 'updated_at'];

	/**
	 * The attributes with geodata
	 * 
	 * @var array
	 */
	protected $geofields = array('location');

	/**
	 * One-toOne relation
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function place()
	{
		return $this->belongsTo('App\Models\Control\Place');
	}

	/**
	 * One-to-One relation
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\Control\User');
	}

	/**
	 * Get locations list with place id
	 * 
	 * @param  int $place_id
	 * @param  int $pagination
	 * @return Illuminate\Pagination\LengthAwarePaginator
	 */
	public function listWithPlaceId($place_id, $pagination)
	{
		$query = $this->query()
			->where('place_id', '=', $place_id, 'and' )
			->orderBy('sort', 'asc');

		if ( ! is_null($pagination))
			$location = $query->paginate($pagination);
		else
			$location = $query->get();

		$location->load(['place']);

		return $location;
	}

	/**
	 * Get info list with place id
	 * 
	 * @param  int $place_id
	 * @param  int $pagination
	 * @return Illuminate\Pagination\LengthAwarePaginator
	 */
	public function listActiveWithPlaceId($place_id, $pagination)
	{
		$query = $this->query()
			->where('place_id', '=', $place_id, 'and' )
			->where('is_active', '=', 1, 'and' )
			->orderBy('sort', 'asc');

		if ( ! is_null($pagination))
			$location = $query->paginate($pagination);
		else
			$location = $query->get();

		$location->load(['place']);

		return $location;
	}

	/**
	 * Get Location addresses
	 * 
	 * @param  int $place_id
	 * @return array
	 */
	public function getAddressesWithPlaceId($place_id)
	{
		return $this->query()
			->where('place_id', '=', $place_id, 'and' )
			->where('is_active', '=', 1, 'and' )
			->orderBy('sort', 'asc')
			->lists('address', 'id')
			->all();
	}

	/**
	 * Get the nearest location
	 * 
	 * @param  int $place_id
	 * @param  float $latitude
	 * @param  float $longitude
	 * @return Model\Control\Location
	 */
	public function getNearestAddress($place_id, $latitude, $longitude)
	{
		if (empty($place_id) || empty($latitude) || empty($longitude))
			return false;

		return DB::table($this->table)
			->select()
			->selectRaw('(6371 * acos( cos( radians(?) ) * cos( radians( X(location) ) ) * cos( radians( Y(location) ) - radians(?) ) + sin( radians(?) ) * sin(radians(X(location))) ) ) AS distance', [$latitude, $longitude, $latitude])
			->where('place_id', '=', $place_id)
			->where('is_active', '=', 1)
			->orderBy('distance')
			->first();
	}

	/**
	 * Unserialize name attribute
	 * 
	 * @param  string
	 * @return string
	 */
	public function getNameAttribute($value)
	{
		return @unserialize($value);
	}

	/**
	 * Serialize name attribute
	 * 
	 * @param  string
	 */
	public function setNameAttribute($value)
	{
		$this->attributes['name'] = serialize($value);
	}

	/**
	 * Unserialize description attribute
	 * 
	 * @param  string
	 * @return string
	 */
	public function getDescriptionAttribute($value)
	{
		return @unserialize($value);
	}

	/**
	 * Serialize description attribute
	 * 
	 * @param  string
	 */
	public function setDescriptionAttribute($value)
	{
		$this->attributes['description'] = serialize($value);
	}

	/**
	 * Prepare coordinates
	 * 
	 * @param  strin $value
	 * @return string
	 */
	public function getLocationAttribute($value)
	{
		$loc = substr($value, 6);
		$loc = preg_replace('/[ ,]+/', ',', $loc, 1);
 
		return substr($loc,0,-1);
	}

	/**
	 * Prepare coordinates for sql
	 * 
	 * @param string $value
	 */
	public function setLocationAttribute($value)
	{
		$this->attributes['location'] = DB::raw("POINT($value)");
	}

	/**
	 * Prepare getting binary coordinates
	 * 
	 * @param  boolean $excludeDeleted
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function newQuery($excludeDeleted = true)
	{
		$raw='';

		foreach($this->geofields as $column)
			$raw .= ' ASTEXT(' . $column . ') As ' . $column . ' ';

		return parent::newQuery($excludeDeleted)->addSelect('*', DB::raw($raw));
	}
}
