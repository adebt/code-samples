<?php namespace App\Models\Control;

use Carbon\Carbon;

use Session;
use Setting;

class Basket {

	/**
	 * Delivery Price
	 */
	const DELIVERY_PRICE = 20;

	/**
	 * Total price for free delivery
	 */
	const DELIVERY_SKIP = 100;

	/**
	 * Session prefix
	 * 
	 * @var string
	 */
	protected $prefix = 'basket';

	/**
	 * Get basket content
	 * 
	 * @return array
	 */
	public function content()
	{
		return (array)Session::get($this->prefix);
	}

	/**
	 * Check if are weight-dish in basket
	 *
	 * @return bool
	 */
	public function hasWeightable()
	{
		foreach( $this->content() as $item )
		{
			if ($item['dish']->is_weight && $item['status'])
				return true;
		}

		return false;
	}

	/**
	 * Get basket total price
	 * 
	 * @return int|float
	 */
	public function total()
	{
		$total = 0;

		foreach( $this->content() as $item )
		{
			if ( ! $item['status'])
				continue;

			if ($item['dish']->is_weight)
			{
				if (is_numeric($item['amount']))
					$item['amount'] = $item['amount'] / 100;
				else
					$item['amount'] = 0;
			}

			if ( ! empty($item['components']['additional']))
			{
				$item['price-additional'] = 0;
				foreach($item['components']['additional'] as $key => $component_id)
				{
					if ($item['components']['additional-status'][$key])
						$item['price-additional'] += $item['components']['additional-price'][$key];
				}
			}
			else
			{
				$item['price-additional'] = 0;
			}

			$total += ($item['price'] + $item['price-additional']) * $item['amount'];
		}

		return number_format($total, 2, '.', '');
	}


	/**
	 * Package price
	 * 
	 * @return int|float
	 */
	public function package()
	{
		$packages = [];

		foreach($this->content() as $item)
		{
			if ( ! $item['status'])
				continue;

			$package = null;
			if( ! empty($item['dish']->package->price))
				$package = $item['dish']->package;
			elseif( ! empty($item['dish']->category->package->price))
				$package = $item['dish']->category->package;
			elseif ( ! empty($item['dish']->category->parent->package->price))
				$package = $item['dish']->category->parent->package;

			if ( ! is_null($package))
			{
				if ($package->single)
				{
					$packages[$package->id] = $package->price;
				}
				else
				{
					if ( ! isset($packages[$package->id]) )
						$packages[$package->id] = $package->price * $item['amount'];
					else
						$packages[$package->id] += $package->price * $item['amount'];
				}
			}
		}

		return number_format(array_sum($packages), 2, '.', '');
	}

	/**
	 * Dishes + packages sum
	 * 
	 * @return int|float
	 */
	public function sum()
	{
		return number_format(($this->total() + $this->package()), 2, '.', '');
	}

	/**
	 * Delivery price
	 * 
	 * @return int|float
	 */
	public function delivery()
	{
		$delivery = Setting::val('order-delivery-price', self::DELIVERY_PRICE);
		if ($this->sum() >= Setting::val('order-delivery-skip', self::DELIVERY_SKIP))
			$delivery = 0;

		return number_format($delivery, 2, '.', '');
	}

	/**
	 * Pay price
	 * 
	 * @return int|float
	 */
	public function pay()
	{
		$pay = $this->delivery() + $this->package() + $this->total();

		return number_format($pay, 2, '.', '');
	}

	/**
	 * Get basket dishes count
	 * 
	 * @return int
	 */
	public function count()
	{
		return count(Session::get($this->prefix));
	}

	/**
	 * Get item key
	 * 
	 * @param  array $data
	 * @return string
	 */
	public function key($data)
	{
		return $this->prefix . '.' . md5(serialize([$data['dish']->id, $data['portion'], $data['components'], $data['comment']]));
	}

	/**
	 * Add dish to basket
	 * 
	 * @param App\Models\Control\Dish $dish
	 * @param int $portion
	 * @param int $amount
	 * @param int|float $price
	 * @param array  $components
	 * @param string $comment
	 * @return void
	 */
	public function add($dish, $portion, $amount, $price, $components = array(), $comment = '')
	{
		$item = [
			'dish' => $dish,
			'portion' => $portion,
			'components' => $components,
			'comment' => $comment,
			'amount' => $amount,
			'price' => $price,
			'status' => 1
		];

		$key = $this->key($item);

		if( Session::has($key) )
			$item['amount'] = $item['amount'] + Session::get($key . '.amount');

		Session::put($key, $item);
	}

	/**
	 * Move basket item to another key
	 * 
	 * @param  string $from
	 * @param  string $to
	 * @return void
	 */
	public function move($from, $to)
	{
		if( ! Session::has($from) )
		{
			return;
		}

		if(Session::has($to))
		{
			$exist_from = $this->get($from);
			$exist_to = $this->get($to);

			$amount = $exist_to['amount'] + $exist_from['amount'];
			Session::put($to . '.amount', $amount);
		}

		if( ! Session::has($to))
		{
			Session::put($to, $this->get($from));
		}

		Session::forget($from);
	}

	/**
	 * Change basket item
	 * 
	 * @param  string $key
	 * @param  array $data
	 * @return void
	 */
	public function change($key, $data)
	{
		$exist = $this->prefix . '.' . $key;
		if( ! Session::has($exist) )
			return;
		
		foreach($data as $param => $value)
			Session::put($exist . '.' . $param, $value);

		$new = $this->key($this->get($key));

		if ($new !== $exist)
			$this->move($exist, $new);
	}

	/**
	 * Get item from basket
	 * 
	 * @param  string $key
	 * @return array
	 */
	public function get($key)
	{
		if ( ! stristr($key, $this->prefix))
			$key = $this->prefix . '.' . $key;

		if( ! Session::has($key))
			return array();

		return (array)Session::get($key);
	}

	/**
	 * Get item price with additional components
	 * 
	 * @param  string $key
	 * @return int|float
	 */
	public function price($key, $only_additional=false)
	{
		$item = $this->get($key);
		if (empty($item))
			return number_format(0, 2, '.', '');

		$price = $only_additional ? 0 : $item['price'];
		if ( ! empty($item['components']['additional']))
		{
			foreach($item['components']['additional'] as $key => $component_id)
			{
				if ($item['components']['additional-status'][$key])
					$price += $item['components']['additional-price'][$key];
			}
		}

		return number_format($price, 2, '.', '');
	}

	/**
	 * Check dish is in basket
	 * 
	 * @param  App\Models\Control\Dish $dish
	 * @return bool
	 */
	public function in($dish)
	{
		foreach( $this->content() as $item )
		{
			if ($item['dish']->id == $dish->id)
				return true;
		}

		return false;
	}

	/**
	 * Clear basket
	 * 
	 * @return void
	 */
	public function clear()
	{
		Session::forget($this->prefix);
	}

	/**
	 * Check if delivery allowed
	 * 
	 * @return boolean
	 */
	public function allowedDelivery()
	{
		list($begin, $end) = explode('-', Setting::val('order-delivery-time', '00:00-00:00') );
		list($bh, $bm) = explode(':', $begin);
		list($eh, $em) = explode(':', $end);

		$begin = Carbon::createFromTime($bh, $bm);
		$end = Carbon::createFromTime($eh, $em);
		$now = Carbon::now();

		return $now->between($begin, $end);
	}
}
