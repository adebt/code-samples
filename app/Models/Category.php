<?php namespace App\Models\Control;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Category extends Model implements StaplerableInterface {

	use SoftDeletes, EloquentTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['parent_id', 'place_id', 'package_id', 'component_group_id', 'name', 'description', 'sort', 'tcategory', 'is_active'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * Allowed dishes sort
	 * 
	 * @var array
	 */
	protected $dish_sort = [
		'price' => 'price',
		'weight' => 'count',
		'calorie' => 'calorie',
		'rating' => 'rating',
	];

	/**
	 * Category constructor
	 * 
	 * @param array $attributes
	 */
	public function __construct(array $attributes = array()) {
		$this->hasAttachedFile('tcategory');

		parent::__construct($attributes);
	}

	/**
	 * One-toOne relation
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function place()
	{
		return $this->belongsTo('App\Models\Control\Place');
	}

	/**
	 * One-to-One relation with categtories
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function parent()
	{
		return $this->hasOne('App\Models\Control\Category', 'id', 'parent_id');
	}

	/**
	 * One-to-Many relation with dishes
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function dishes()
	{
		return $this->hasMany('App\Models\Control\Dish');
	}

	/**
	 * One-to-One relation with packages
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function package()
	{
		return $this->belongsTo('App\Models\Control\Package');
	}

	/**
	 * One-to-One relation with component groups
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function group()
	{
		return $this->belongsTo('App\Models\Control\ComponentGroup', 'component_group_id', 'id');
	}

	/**
	 * Get categories list with place id
	 * 
	 * @param  int $place_id
	 * @param  array $params
	 * @param  int $pagination
	 * @return Illuminate\Pagination\LengthAwarePaginator
	 */
	public function listWithPlaceId($place_id, $params, $pagination)
	{
		$query = $this->query()
			->where('place_id', '=', $place_id, 'and' )
			->orderBy('sort', 'asc');

		if ( ! empty($params['parent_id']))
		{
			$query->where('parent_id', '=', $params['parent_id']);
		}

		if ( ! empty($params['is_active']))
		{
			$query->where('is_active', '=', ($params['is_active'] == 'yes') ? 1 : 0);
		}

		if ( ! empty($params['s']))
		{
			$s = '%' . trim($params['s']) . '%';
			$query->where(function($query) use ($s){
				$query->where('name', 'LIKE', $s)
					->orWhere('description', 'LIKE', $s);
			});
		}

		if ( ! is_null($pagination))
			$categories = $query->paginate($pagination);
		else
			$categories = $query->get();

		$categories->load(['place', 'parent', 'dishes', 'dishes.place', 'dishes.portions', 'dishes.components']);

		return $categories;
	}

	/**
	 * Get categories list with place id
	 * 
	 * @param  int $place_id
	 * @param  int $pagination
	 * @return Illuminate\Pagination\LengthAwarePaginator
	 */
	public function listActiveWithPlaceId($place_id, $pagination)
	{
		$query = $this->query()
			->where('place_id', '=', $place_id, 'and' )
			->where('is_active', '=', 1, 'and')
			->orderBy('sort', 'asc');

		if ( ! is_null($pagination))
			$categories = $query->paginate($pagination);
		else
			$categories = $query->get();
		
		$categories->load(['place', 'parent', 'dishes', 'dishes.place', 'dishes.portions', 'dishes.components']);

		return $categories;
	}

	/**
	 * Get categories list with place id and parent id
	 * 
	 * @param  int $place_id
	 * @param  int $parent_id
	 * @param  int $pagination
	 * @return Illuminate\Pagination\LengthAwarePaginator
	 */
	public function listActiveWithPlaceIdAndParentId($place_id, $parent_id, $pagination)
	{
		$query = $this->query()
			->where('place_id', '=', $place_id, 'and' )
			->where('parent_id', '=', $parent_id, 'and' )
			->where('is_active', '=', 1, 'and')
			->orderBy('sort', 'asc');

		if ( ! is_null($pagination))
			$categories = $query->paginate($pagination);
		else
			$categories = $query->get();
		
		$categories->load(['place', 'parent', 'dishes', 'dishes.place', 'dishes.portions', 'dishes.components', 'dishes.favorite']);

		return $categories;
	}

	/**
	 * Prepare categories list for select
	 * 
	 * @return array
	 */
	public function prepareCategories($place_id, $category_skip_id=null)
	{
		$categories = $this->query()
			->where('place_id', '=', $place_id, 'and' )
			->orderBy('sort', 'asc');

		if ( ! is_null($category_skip_id))
			$categories->where('id', '<>', $category_skip_id, 'and');
		
		$result = $categories->lists('name', 'id')->all();
		foreach ( $result as &$value )
			$value = $value[config('laravel-gettext.locale')];

		return $result;
	}

	/**
	 * Sort dishes in category
	 * 
	 * @param  Illuminate\Database\Eloquent\Collection &$categories
	 * @param  string $sort
	 */
	public function sortDishes(&$categories, $sort)
	{
		if ( array_key_exists($sort, $this->dish_sort))
		{
			$sort = $this->dish_sort[$sort];

			foreach ($categories as &$category)
			{
				$dishes = $category->dishes;
				$dishes = $dishes->sortBy(function($dish) use ($sort){
					if ( $sort == 'rating' )
					{
						return $dish->$sort;
					}
					else
					{
						$default = $dish->portionDefault();

						if ( ! empty($default))
							return $default->$sort;
					}
				}, SORT_REGULAR, ($sort == 'rating'));

				$category->dishes = $dishes;
			}
		}
	}

	/**
	 * Get group components
	 *
	 * @return array
	 */
	public function getComponents()
	{
		if( ! empty($this->group->components))
			return $this->group->components;
		elseif ( ! empty($this->group->parent->group->components))
			return $this->group->parent->group->components;
		else
			return [];
	}

	/**
	 * Unserialize name attribute
	 * 
	 * @param  string
	 * @return string
	 */
	public function getNameAttribute($value)
	{
		return @unserialize($value);
	}

	/**
	 * Serialize name attribute
	 * 
	 * @param  string
	 */
	public function setNameAttribute($value)
	{
		$this->attributes['name'] = serialize($value);
	}

	/**
	 * Unserialize description attribute
	 * 
	 * @param  string
	 * @return string
	 */
	public function getDescriptionAttribute($value)
	{
		return @unserialize($value);
	}

	/**
	 * Serialize description attribute
	 * 
	 * @param  string
	 */
	public function setDescriptionAttribute($value)
	{
		$this->attributes['description'] = serialize($value);
	}

}
